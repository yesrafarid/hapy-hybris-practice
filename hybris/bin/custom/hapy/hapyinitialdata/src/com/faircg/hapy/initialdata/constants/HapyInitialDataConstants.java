/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.faircg.hapy.initialdata.constants;

/**
 * Global class for all HapyInitialData constants.
 */
public final class HapyInitialDataConstants extends GeneratedHapyInitialDataConstants
{
	public static final String EXTENSIONNAME = "hapyinitialdata";

	private HapyInitialDataConstants()
	{
		//empty
	}
}
